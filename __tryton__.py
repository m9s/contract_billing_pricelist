# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Billing Pricelist',
    'name_de_DE': 'Vertragsverwaltung Abrechnung Preislisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds pricelist to billing of contracts
    ''',
    'description_de_DE': '''
    - Fügt Preislisten zur Abrechnung von Verträgen hinzu.
    ''',
    'depends': [
        'account_invoice_billing_pricelist',
        'contract_billing'
    ],
    'xml': [
        'contract.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
