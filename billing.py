#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL

class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    def _get_pricelist_from_line(self, line):
        res = super(BillingLine, self)._get_pricelist_from_line(line)
        if line.contract:
            res = line.contract.pricelist
        return res

BillingLine()
