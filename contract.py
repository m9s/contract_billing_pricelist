# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pyson import Eval, Not, Equal


class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    pricelist = fields.Many2One('pricelist.pricelist', 'Pricelist',
            states={
                'required': True,
                'readonly': Not(Equal(Eval('state'), 'draft'))
            },
            depends=['state'])

    def _get_invoice_contract(self, contract):
        res = super(Contract, self)._get_invoice_contract(contract)
        res['pricelist'] = contract.pricelist.id
        return res

Contract()
